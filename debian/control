Source: golang-github-bazelbuild-bazelisk
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Arthur Diniz <arthurbdiniz@gmail.com>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-bgentry-go-netrc-dev,
               golang-github-mitchellh-go-homedir-dev,
               golang-github-hashicorp-go-version-dev,
               help2man <!nodoc>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-bazelbuild-bazelisk
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-bazelbuild-bazelisk.git
Homepage: https://github.com/bazelbuild/bazelisk
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/bazelbuild/bazelisk

Package: bazelisk
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: user-friendly launcher for Bazel
 This is the CLI package for Bazelisk, a wrapper for Bazel written in Go.
 .
 It automatically picks a good version of Bazel given your current working
 directory, downloads it from the official server (if required) and then
 transparently passes through all command-line arguments to the real Bazel
 binary.
 .
 You can call it just like you would call Bazel.

Package: golang-github-bazelbuild-bazelisk-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: user-friendly launcher for Bazel (library)
 This is the library package for Bazelisk.
 .
 It can be used in golang files using the following go import path:
 github.com/bazelbuild/bazelisk
 .
 In case you just want the CLI try the binary package called bazelisk.
